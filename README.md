## Infinity For Reddit Auto-builder

### Table of Contents

- [Infinity For Reddit Auto-builder](#infinity-for-reddit-auto-builder)
  - [Table of Contents](#table-of-contents)
  - [About this project](#about-this-project)
- [1. Fork this project](#1-fork-this-project)
  - [Updating your fork](#updating-your-fork)
- [2. Create a Reddit API](#2-create-a-reddit-api)
- [3. Setup CI variables](#3-setup-ci-variables)
- [4. Notifications](#4-notifications)
  - [Ntfy (Difficulty: Easy)](#ntfy-difficulty-easy)
  - [Telegram (Difficulty: Medium)](#telegram-difficulty-medium)
- [5. Scheduling automated builds](#5-scheduling-automated-builds)
- [6. (Optional) Manual trigger build](#6-optional-manual-trigger-build)
- [(Optional) Force version build](#optional-force-version-build)
- [(Optional) Your own keystore for app signature](#optional-your-own-keystore-for-app-signature)
- [(Optional) Custom package name](#optional-custom-package-name)
  - [FAQ](#faq)
  - [Credits](#credits)

### About this project

Run automated builds for [Infinity-For-Reddit](https://github.com/Docile-Alligator/Infinity-For-Reddit).  
Setup once and forget.  

CI job runs daily, if there is a new version, it will be uploaded to GitLab and send a notification to **ntfy** or **Telegram**.

### Warning

New GitLab accounts require Credit Card validation to run Pipelines  
<https://forum.gitlab.com/t/concern-about-gitlab-asking-for-credit-card/54479/2>

## 1. Fork this project

1. **Fork this project**  
![Fork](assets/fork.png)
2. **Branches to include:** ``Only the default branch main``
3. **Visibility level:** ``Private``

**NOTE:** Make sure the project is private, otherwise anyone can download and use your APK built with your Reddit API.  
You you didn't make it private, you can change the visibility to ``Private`` in Settings -> Visibility.

#### Updating your fork  

If your fork is outdated, a message will appear on your fork to update it.  
Click ``Update Fork``  
![update fork](assets/update_fork.png)

This may include fixes or new features for building the apk.

## 2. Create a Reddit API

**Create an app Reddit API with the following settings:**

- Go to <https://old.reddit.com/prefs/apps/>
- **name:** ``{YourRedditUsername}s-app`` (Replace ``{YourRedditUsername}`` with your actual username)
- **type:** ``Installed app``
- **redirect uri:** ``http://127.0.0.1``
- **Example:**

  ![](assets/redditapi.png)

- The API token is then shown at the bottom of the page under the App name (``{YourRedditUsername}s-app``) and is a set of random characters (e.g. ``6g5ZHEGEAnKHP3vewUFY3y``)
Copy the API token.

## 3. Setup CI variables

On your Gitlab fork (left panel or on mobile top left icon) open Settings -> CI/CD

1. **Add Reddit API variable**

- Create a new variable: Variables -> Add variable
- **Flags:** check ``Masked``
- **Key:** ``REDDIT_API``
- **Value:** ``<your Reddit API token>``
- **Example:**

![](assets/redditapivar.png)

2. **Add username variable**

- Create a new variable: Variables -> Add variable
- **Flags:** check ``Masked``
- **Key:** ``USERNAME``
- **Value:** ``<your reddit username>``

**NOTE:** If your username is less than 8 characters, leave flag ``Masked`` unchecked.

1. **CI project access token**

  **Note:** CI project token max. lifetime is 1 year.  
  Needs to be removed an redone this step again.

- Settings -> Access Tokens
- Add new token
- **Token name:** ``GL_TOKEN``
- **Expiration date:** blank
- **Select scopes:** ``read_api``
- **Example:**

     ![](assets/gl_token.png)

5. **Add CI project access token variable**

- Create a new variable: Variables -> Add variable
- **Flags:** check ``Masked``
- **Key:** ``GL_TOKEN``
- **Value:** ``<your project access token>``

**NOTE:** If isn't possible to create a Project Aceess Token, create a Personal Access Token instead from <https://gitlab.com/-/user_settings/personal_access_tokens>

6. **(Optional) Skip beta version**

- Create a new variable: Variables -> Add variable
- **Key:** ``IGNORE_BETA``
- **Value:** ``yes``

7. **(Optional) Disable builds scripts sync during build**

**NOTE:** This disables syncing latest build scripts during pipeline builds.
Disable this if don't want to run the latest version of infinity-autobuild during pipeline builds.

- Create a new variable: Variables -> Add variable
- **Key:** ``SYNC_CI``
- **Value:** ``no``

8. **(Optional) Custom GIPHY API KEY**
**NOTE:** Shared GIPHY only allows 100 searches/API calls per hour, in case of limit reached, you can use your own GIPHY API key.

- Go to <https://developers.giphy.com/docs/api/>
- Click on "Create an API key"
- Select SDK
- Create a new variable: Variables -> Add variable
- **Key:** ``GIPHY_API``
- **Value:** ``<your GIPHY API key>``

## 4. Notifications

Receive a notification on when a new version is available.  
Choose one of the following:

### Ntfy (Difficulty: Easy)

1. Install ntfy app ([Google Play](https://play.google.com/store/apps/details?id=io.heckel.ntfy)) ([F-Droid](https://f-droid.org/en/packages/io.heckel.ntfy/))
2. Create a new topic with a unique name (e.g. ``Kais39qjcqIp``)
3. Create a new variable: Variables -> Add variable:

- **Flags:** check ``Masked``
- **Key:** ``NTFY_TOPIC``
- **Value:** ``<your topic name>``
  
**Optional:** Use custom server

- Create a new variable: Variables -> Add variable:
  - **Flags:** check ``Masked``
  - **Key:** ``NTFY_SERVER``
  - **Value:** ``<your ntfy server>``

### Telegram (Difficulty: Medium)

1. Create a new bot with [BotFather](https://t.me/BotFather)

- type ``/new``
- Follow the bot instructions
  
2. Copy the bot token
3. Create a new variable: Variables -> Add variable:

- **Flags:** check ``Masked``
- **Key:** ``TG_TOKEN``
- **Value:** ``<your bot token>``

4. Create a new (private) Channel
5. Create or add the bot to your (private) Channel, and set it as admin.

- Not possible with private chat with the bot.

6. Get the chat id

- Open <http://web.telegram.org> on your browser and open your group or channel
- From URL copy the chat id (e.g. ``https://web.telegram.org/a/#-1001234567890`` The number after ``#`` is your chat id ``-1001234567890``)

7. Create a new variable: Variables -> Add variable:

- **Flags:** check ``Masked``
- **Key:** ``TG_CHATID``
- **Value:** ``<your chat id>``

## 5. Scheduling automated builds

- **Build -> Pipeline schedules**
- **Description:** ``Auto Build``
- **Interval Pattern:** ``Every Day``
- **Cron timezone:** ``<pick your timezone>`` or set ``[UTC-0] UTC``

## 6. (Optional) Manual trigger build

- **Build -> Pipelines**
- **Run pipeline**
- **Run pipeline**

## (Optional) Force version build

- Run a new pipeline.
- Add variable ``RELEASE`` with the version or commit of your chosen version.

**Example:**

- Input variable key: ``RELEASE``
- Input variable value: ``v7.2.9``

See release tags on <https://github.com/Docile-Alligator/Infinity-For-Reddit/releases>

## (Optional) Your own keystore for app signature

**NOTE:** To install an APK with a different keystore signature, you need to remove the previous one.  
Backup your settings before uninstalling.

Create a new keystore file.

- PC: [APK Editor Studio](https://qwertycube.com/apk-editor-studio) or [Android Studio](https://developer.android.com/studio)
- Android: [KeyStore Explorer](https://play.google.com/store/apps/details?id=com.mipoda.aks.explorer)
- Convert keystore file to base64 (<https://base64.guru/converter/encode/file>)
- Create a new variables:
  - **Flags:** check ``Visible``
  - **Key:** ``APK_KS``
  - **Value:** ``<your keystore base64>``
  - **Key:** ``APK_KS_PASS``
  - **Value:** ``<your keystore password>``
  - **Key:** ``APK_KEY_PASS``
  - **Value:** ``<your key password>``
  - **Key:** ``APK_KS_ALIAS``
  - **Value:** ``<your keystore alias>``

## (Optional) Custom package name

Change the package app name (``ml.docilealligator.infinityforreddit``) to a custom name, this way it can be used along side with other custom build or official app from the Play Store.

- Create a new variable: Variables -> Add variable
  - Key: ``PKG_NAME``
  - Value: ``<your custom package name>``

**NOTE:** Numbers and special characters other than dots and underscores are not allowed.
Examples:

- ir.example.app
- com.example.myapp
- org.example.myapp

### FAQ

**Q:** Where can i find previous builds?  
**A:** All previous builds are stored in Deploy -> Package Registry.

**Q:** Why can't the Telegram bot send the apk to private chat?  
**A:** Telegram does not allow you to send files to private chats.

**Q:** Why Telegram bot needs to be admin?  
**A:** Only admins can send files to Telegram channels.

**Q:** Can i use a different pipeline schedule?  
**A:** Yes, you can use other pre-made schedules or a custom cron expression (see <https://crontab.cronhub.io/> for examples).  
Example: ``0 12 */2 * *`` run every 2 days at 12:00PM.

**Q:** My pipelines keeps failing, how can i report it?  
**A:** Go to  Build -> Artifacts, under under "Artifacts" column download the latest ``job.log``, create a new issues on [American_Jesus/infinity-autobuild/-/issues](https://gitlab.com/American_Jesus/infinity-autobuild/-/issues) describing your problem.

**Q:** How can i send notification to other services?  
**A:** Other services aren't supported yet. But you can create an issue requesting what service you want to use.

**Q:** Why notifications by email aren't available?  
**A:** Some email providers don't allow sending APK files by attachment or URL due to security reasons.

### Credits

- [Docile-Alligator/Infinity-For-Reddit](https://github.com/Docile-Alligator/Infinity-For-Reddit)  
**Reddit Users:**
- u/Oha_der_erste
- u/StudyGuidex
- u/aman207
