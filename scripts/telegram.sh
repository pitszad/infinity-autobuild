#!/usr/bin/env bash

set -xT

BOTTOKEN="$TG_TOKEN"
CHATID="$TG_CHATID"
MESSAGE="$1"
APK="$2"

if ! echo "$CHATID" | grep -q "\-100"; then
	CHATID="-100${CHATID//-/}"
fi

# get bot ID
BOT_ID="$(curl -s "https://api.telegram.org/bot${BOTTOKEN}/getMe" | jq '.result.id')"

# check if bot is admin

IS_ADMIN="$(curl -s -F chat_id="$CHATID" -F user_id="$BOT_ID" "https://api.telegram.org/bot${BOTTOKEN}/getChatMember" | jq -r '.result.status')"

if [ "$IS_ADMIN" != "administrator" ]; then
	echo -e "\e[31mTelegram Error: Bot is not an administrator\e[0m"
	echo -e "\e[33mSet bot as administrator"
	exit 1
fi

send_apk() {
	curl -s \
		-F chat_id="$CHATID" \
		-F document=@"$APK" \
		-F caption="$MESSAGE" \
		-F parse_mode="Markdown" \
		"https://api.telegram.org/bot${BOTTOKEN}/sendDocument"
}

if send_apk; then
	echo -e "\n\e[32mINFO: APK sent to Telegram successfully\e[0m"
else
	echo -e "\n\e[31mTelegram Error: Failed to send APK to Telegram\e[0m"
	exit 1
fi
