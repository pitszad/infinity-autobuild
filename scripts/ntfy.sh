#!/bin/bash

set -xT

MESSAGE="$1"
APK_URL="$2"
TOPIC="$NTFY_TOPIC"
SERVER="${NTFY_SERVER:-ntfy.sh}"

send_apk() {
	curl -H "Title: New Infinity version available!" \
		-H "Tags: loudspeaker" \
		-H "Actions: view, Download, ${APK_URL}, clear=true" \
		-d "$MESSAGE
$APK_URL" \
		"$SERVER/$TOPIC"
}

if send_apk; then
	echo -e "\e[32mINFO: APK sent to NTFY successfully\e[0m"
else
	echo -e "\e[31mERROR: Failed to send APK to NTFY\e[0m"
	exit 1
fi
