#!/bin/bash
set -x

[ -n "$SYNC_CI_FORK" ] && SYNC_CI="$SYNC_CI_FORK"

# sync upstream infinity-auto-builder
if [ "$SYNC_CI" != "no" ] && [ "$CI_COMMIT_BRANCH" = "main" ]; then
	git remote add upstream https://gitlab.com/American_Jesus/infinity-autobuild.git
	git fetch upstream
	git checkout "origin/$CI_COMMIT_BRANCH"
	git merge "upstream/$CI_COMMIT_BRANCH"
fi

